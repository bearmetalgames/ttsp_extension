#include "gpio_controller.hpp"

#include <string>
#include <thread>

#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/variant/utility_functions.hpp>


using namespace godot;

void GPIOController::_bind_methods()
{
	ClassDB::bind_method(D_METHOD("set_high"), &GPIOController::set_high);
	ClassDB::bind_method(D_METHOD("set_low"), &GPIOController::set_low);
	ClassDB::bind_method(D_METHOD("set_pin", "pinNum", "gpioDir"), &GPIOController::set_pin);
}

GPIOController::GPIOController()
{
#ifdef __x86_64__ 
	is_arm = false;
    UtilityFunctions::print("X64 system"); 
#else    
    UtilityFunctions::print("ARM system");
	is_arm = true;
#endif  
}

GPIOController::~GPIOController()
{
	for (const auto& kv : gpioFPMap) {
		kv.second->close();
	}
}

void GPIOController::_ready()
{
}

void GPIOController::_process(double delta)
{
}

void GPIOController::set_pin(int pinNum, String gpioDir)
{
	int pathNum = BASE_GPIO + pinNum;
	std::string gpioPath = "/sys/class/gpio/gpio" + std::to_string(pathNum) + "/";
	UtilityFunctions::print(String(gpioPath.c_str())); 

	std::fstream exportSetup;
	exportSetup.open("/sys/class/gpio/export", std::ios::out);
	exportSetup << std::to_string(pathNum);
	exportSetup.close();

	std::fstream dirSetup;
	dirSetup.open(gpioPath + "direction", std::ios::out);
	dirSetup << gpioDir.utf8().get_data();

	std::fstream* pinFile = new std::fstream(gpioPath + "value");
	gpioFPMap.insert({ pinNum, pinFile });
}

void GPIOController::set_high(int pin)
{
	std::fstream* pinFile = gpioFPMap[pin];
	pinFile->write("1", 1);
}

void GPIOController::set_low(int pin)
{
	std::fstream* pinFile = gpioFPMap[pin];
	pinFile->write("0", 1);
}

// 1. Make this work in a thread. And it will wait until the gpioPath exists
// 2. Make a map that is thread -> pinNum. That way set_high and set_low will wait on those to set
// 3. Setup the getters for the pins 
void GPIOController::setup_pin_files(int pinNum, std::string gpioPath, std::string gpioDir) {
	std::fstream dirSetup;
	dirSetup.open(gpioPath + "direction", std::ios::out);
	dirSetup << gpioDir;

	std::fstream* pinFile = new std::fstream(gpioPath + "value");
	gpioFPMap.insert({ pinNum, pinFile });
}