#pragma once
#include <iostream>
#include <fstream>
#include <map>

#include <godot_cpp/classes/node.hpp>
#include <godot_cpp/core/class_db.hpp>

/**
 * @brief GPIOController
 * 
 * This will control the GPIO pins on a Raspberry Pi
 * 
 * It will do this using the sys file interface and writing to those files
 * 
 */

using namespace godot;

enum GPIODirection {
	IN,
	OUT,
};

class GPIOController : public Node
{
	GDCLASS(GPIOController, Node);

protected:
	static void _bind_methods();

public:
	GPIOController();
	~GPIOController();

	void set_pin(int, String);

	void _ready() override;
	void _process(double delta) override;

	void set_high(int pin);
    void set_low(int pin);

// I think I will want a map: (pin_string, in_or_out, file_pointer)
private:
	// All file names go up from 512 (gpio0 = 512, gpio1 = 513, etc)
	const int BASE_GPIO = 512;
    bool is_arm = false;
	std::map<int, std::string> gpioPathMap;
	std::map<int, std::fstream*> gpioFPMap;

	void setup_pin_files(int pinNum, std::string gpioPath, std::string gpioDir);
};