extends Button

var is_high: bool
var gpio_controller: GPIOController

# Called when the node enters the scene tree for the first time.
func _ready():
	is_high = false
	self.pressed.connect(button_press)
	gpio_controller = GPIOController.new()
	gpio_controller.set_pin(22, "out")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func button_press():
	if is_high:
		gpio_controller.set_high(22)
	else:
		gpio_controller.set_low(22)

	is_high = not is_high
	
